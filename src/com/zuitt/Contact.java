package com.zuitt;
import java.util.ArrayList;

public class Contact {
    private String name;
    ArrayList<String> numbers = new ArrayList<>();
    ArrayList<String> addresses = new ArrayList<>();


    //parameterized constructor
    public Contact (String name, ArrayList<String> numbers, ArrayList<String> addresses)
    {
        this.name = name;
        this.numbers = numbers;
        this.addresses = addresses;
    }

    public Contact(String name) {
        this.name = name;
    }

    //getters
    public String getName(){
        return this.name;
    }

    public ArrayList<String> getNumbers() {

        return this.numbers;
    }

    public ArrayList<String> getAddresses() {
        return this.addresses;
    }

    //setters
    public String setName (String name){
        return this.name = name;
    }

    public ArrayList<String> setNumbers(ArrayList<String> numbers){
        numbers.add("0922-222-2222");
        numbers.add("0911-111-1111");
        return this.numbers = numbers;
    }

    public ArrayList<String> setAddresses(ArrayList<String> addresses){
        addresses.add("Makati City");
        addresses.add("Quezon City");
        return this.addresses = addresses;
    }
}
