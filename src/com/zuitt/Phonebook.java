package com.zuitt;
import java.util.ArrayList;
public class Phonebook{
    ArrayList<String> contacts = new ArrayList<>();
    private Contact c;

    //parameterized constructor
    public Phonebook(ArrayList<String> contacts)
    {
        ;
        contacts.add("Jane Doe");
        this.contacts = contacts;
    }

    public Phonebook(){
        this.c = new Contact("John Doe");
        this.c = new Contact("Jane Doe");
    }

    //getter
    public String getContact(){
        return this.c.getName();
    }
    public ArrayList<String> getContacts() {
        return this.contacts;
    }

    //setter
    public ArrayList<String> setContacts(ArrayList<String> contacts){
        return this.contacts = contacts;
    }

    public void person(){
        System.out.println(getContact()+" has the following numbers");
    }
}
